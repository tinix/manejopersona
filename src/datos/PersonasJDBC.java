package datos;

import domain.Persona;

import java.sql.*;
import java.util.*;


public class PersonasJDBC {
	
	private final String SQL_INSERT = "INSERT INTO persona(nombre, apellido) VALUE(?,?)"; 
	//corresponde a cada unos de los valores ?nombre apellido?
	
	private final String SQL_UPDATE = "UPDATE persona SET nombre=?, apellido=?, WHERE id_persona";
	
	private final String SQL_DELETE = "DELETE FROM persona WHERE id_persona";
	
	private final String SQL_SELECT = "SELECT id_persona, nombre, apellido FROM persona ORDER BY id_persona";
	
	public int insert(String nombre, String apellido) {
		Connection conn  = null;
		PreparedStatement stmt = null;
		ResultSet rs = null;
		
		int rows = 0;
		try {
			conn = Conexion.getConnection(); // esto regresa una connection viva a la DB;
			stmt = conn.prepareStatement(SQL_INSERT);
			int index = 1;
			stmt.setString(index++, nombre);
			stmt.setString(index++, apellido);
			System.out.println("Ejecutando query: "+ SQL_INSERT);
			rows = stmt.executeUpdate();//no registro afectados
			
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			Conexion.close(stmt);
			Conexion.close(conn);
		}
		
		return rows;
	}
	
	/**
	 * Este metodo hace una actualizacion en la DB personas
	 * @param id_persona
	 * @param nombre
	 * @param apellido
	 * @return int. 1 si salio todo correcto.
	 * @author tinix
	 */
	public int update(int id_persona, String nombre, String apellido) {
		Connection conn = null;
		PreparedStatement stmt = null;
		int rows = 0;
		try {
			conn = Conexion.getConnection();
			System.out.println("Ejecutando query" + SQL_UPDATE);
			stmt = conn.prepareStatement(SQL_UPDATE);
			int index = 1;
			stmt.setString(index++, nombre);
			stmt.setString(index++, apellido);
			stmt.setInt(index, id_persona );
			rows = stmt.executeUpdate();
			System.out.println("Registro actualizado" + rows);
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			Conexion.close(stmt);
			Conexion.close(conn);
		}
		return rows;
	}
	
	/**
	 * 
	 * @param id_persona
	 * @return int rows
	 * @author tinix;
	 */
	public int delete(int id_persona) {
		Connection conn = null;
		PreparedStatement stmt = null;
		int rows = 0;
		try {
			conn = Conexion.getConnection();
			System.out.println("Ejecutando query :" + SQL_DELETE);
			stmt = conn.prepareStatement(SQL_DELETE);
			stmt.setInt(1, id_persona);
			rows = stmt.executeUpdate();
			System.out.println("Registros Eliminados");
			
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			Conexion.close(stmt);
			Conexion.close(conn);
		}
		
		return rows;
	}
	
	/**
	 * 
	 * @return personas
	 */
	public List<Persona> select() {
		Connection conn = null;
		PreparedStatement stmt = null;
		ResultSet resultset = null;
		Persona persona = null;
		List<Persona> personas = new ArrayList<Persona>();
		try {
			conn = Conexion.getConnection();
			stmt = conn.prepareStatement(SQL_SELECT);
			resultset = stmt.executeQuery();
			while(resultset.next()) {
				int id_persona = resultset.getInt(1);
				String nombre = resultset.getString(2);
				String apellido = resultset.getString(3);
				persona = new Persona();
				persona.setId_persona(id_persona);
				persona.setNombre(nombre);
				persona.setApellido(apellido);
				personas.add(persona);
			}
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			Conexion.close(resultset);
			Conexion.close(stmt);
			Conexion.close(conn);
		}
		return personas;
	} 
}
